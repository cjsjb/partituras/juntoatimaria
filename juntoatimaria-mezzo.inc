\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/2
		\clef "treble"
		\key d \major

		R1*4  |
%% 5
		d' 4 d' e' fis' 8 fis' ~  |
		fis' 8 e' 2 r8 d' cis'  |
		d' 4 d' e' fis'  |
		cis' 2.. r8  |
		b 4 cis' d' b 8 a ~  |
%% 10
		a 4 d' d' fis'  |
		e' 4 b cis' d' 8 d' ~  |
		d' 8 ( cis' 2. ) r8  |
		d' 4 d' e' fis' 8 fis' ~  |
		fis' 8 e' 2 r8 d' cis'  |
%% 15
		d' 4 d' e' fis'  |
		cis' 2.. r8  |
		b 4 cis' d' b 8 a ~  |
		a 4 d' d' fis'  |
		e' 2. -\staccato d' 4  |
%% 20
		d' 1 ~  |
		d' 2 r  |
		d' 1  |
		cis' 1  |
		d' 1  |
%% 25
		cis' 2.. r8  |
		d' 1  |
		d' 1  |
		d' 1  |
		cis' 2.. r8  |
%% 30
		d' 1  |
		cis' 1  |
		d' 1  |
		cis' 2.. r8  |
		d' 1  |
%% 35
		d' 1  |
		d' 1  |
		a 1 ~  |
		a 2 r  |
		d' 4 d' e' fis' 8 fis' ~  |
%% 40
		fis' 8 e' 2 r8 d' cis'  |
		d' 4 d' e' fis'  |
		cis' 2.. r8  |
		b 4 cis' d' b 8 a ~  |
		a 4 d' d' fis'  |
%% 45
		e' 4 b cis' d' 8 d' ~  |
		d' 8 ( cis' 2. ) r8  |
		d' 4 d' e' fis' 8 fis' ~  |
		fis' 8 e' 2 r8 d' cis'  |
		d' 4 d' e' fis'  |
%% 50
		cis' 2.. r8  |
		b 4 cis' d' b 8 a ~  |
		a 4 d' d' fis'  |
		e' 2. -\staccato d' 4  |
		d' 1 ~  |
%% 55
		d' 2 r  |
		d' 1  |
		cis' 1  |
		d' 1  |
		cis' 2.. r8  |
%% 60
		d' 1  |
		d' 1  |
		d' 1  |
		cis' 2.. r8  |
		d' 1  |
%% 65
		cis' 1  |
		d' 1  |
		cis' 2.. r8  |
		d' 1  |
		d' 1  |
%% 70
		d' 1  |
		a 1 ~  |
		a 2 r  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
Jun -- "to a" ti, Ma -- rí -- a,
co -- mo ni -- ño quie -- "ro es" -- tar.
Tó -- ma -- "me en" tus bra -- zos,
guí -- a -- "me en" mi ca -- mi -- nar. __

Quie -- ro que "me e" -- du -- ques,
que "me en" -- se -- ñes a re -- zar.
Haz -- me trans -- pa -- ren -- te,
llé -- na -- me de paz. __

Ma -- dre, ma -- dre, ma -- dre, ma -- dre.
Ma -- dre, ma -- dre, ma -- dre, ma -- dre. __

Gra -- cias, ma -- dre mí -- a,
por lle -- var -- nos a Je -- sús.
Haz -- nos más hu -- mil -- des,
tan sen -- ci -- llos co -- mo tú. __

Gra -- cias, ma -- dre mí -- a,
por a -- brir tu co -- ra -- zón,
por -- que nos con -- gre -- gas
y nos das "tu a" -- mor. __

Ma -- dre, ma -- dre, ma -- dre, ma -- dre.
Ma -- dre, ma -- dre, ma -- dre, ma -- dre. __
	}
>>
