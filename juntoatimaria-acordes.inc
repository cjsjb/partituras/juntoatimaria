\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		d1 a1 d1 a1

		% junto a ti, maria...
		d1 a1 b1:m fis1:m
		g1 d1 e1:m a1

		d1 a1 b1:m fis1:m
		g1 d1 e2:m a2 d1 d1

		% madre...
		d1 a1 b1:m fis1:m
		g1 d1 e1:m a1

		d1 a1 b1:m fis1:m
		g1 d1 e2:m a2 d1 d1

		% gracias, madre mia...
		d1 a1 b1:m fis1:m
		g1 d1 e1:m a1

		d1 a1 b1:m fis1:m
		g1 d1 e2:m a2 d1 d1

		% madre...
		d1 a1 b1:m fis1:m
		g1 d1 e1:m a1

		d1 a1 b1:m fis1:m
		g1 d1 e2:m a2 d1 d1
	}
